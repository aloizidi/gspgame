﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;

namespace gspgame
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine($"{nameof(Main)}: starting.");
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();
            builder.Services.AddSingleton<gspgame.Data.WeatherForecastService>();
            builder.Services.AddSingleton<IEmailSender, EmailSender>();
            builder.Services.AddSingleton<IDatabaseProxy, DatabaseProxy>();
            builder.Services.AddSingleton<IMatchReader, MatchReader>();
            builder.Services.AddSingleton<IPrediction, Prediction>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            // // logging
            // var appSettingFilename = app.Environment.IsDevelopment() ? 
            //     "appsettings.Development.json" : "appsettings.json";
            // var config = new ConfigurationBuilder()
            //     .AddJsonFile(appSettingFilename, optional: true, reloadOnChange: true)
            //     .Build();
            // LogManager.Configuration = new NLogLoggingConfiguration(config.GetSection("NLog"));
            // var logger = LogManager.GetCurrentClassLogger();

            try
            {
                //logger.Debug("Init main");
                Console.WriteLine($"{nameof(Main)}: starting app.");
                DatabaseProxy proxy = new DatabaseProxy();
               // proxy.GetMatchesOfThisWeekAsync().Wait();
               
                app.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{nameof(Main)}: app failed with exception {ex.Message}");
                //logger.Error(ex, "Stopped program because of exception");
            }
        }
    }
}
