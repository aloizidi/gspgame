FROM mcr.microsoft.com/dotnet/sdk:7.0 AS base
WORKDIR /app
EXPOSE 5000

ENV ASPNETCORE_URLS=http://+:5000

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-dotnet-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser
FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["gspgame.csproj", "./"]
RUN dotnet restore "gspgame.csproj" --no-cache
RUN dotnet nuget locals all --clear
COPY . .
WORKDIR "/src/."
RUN dotnet build "gspgame.csproj" -c Release -o /app/build --no-incremental /M:1 -v diag

FROM build AS publish
RUN dotnet publish "gspgame.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "gspgame.dll"]
