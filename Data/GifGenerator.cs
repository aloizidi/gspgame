using System;

public class GifGenerator
{
    private const int numberOfGifs = 9;
    private const string baseGifPath = "FunnyGifs/fun";
    private const string gifExtension = ".gif";
    private Random rnd = new Random();

    public string GetRandomGifPath()
    {
        int randomNumber = rnd.Next(1, numberOfGifs + 1);
        return baseGifPath + randomNumber.ToString() + gifExtension;
    }
}