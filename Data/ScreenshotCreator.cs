
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using ImageMagick;

public class ScreenshotCreator : IScreenshotCreator
{
    private string imageExtension = ".jpg";
    private string screenshotPath;
    private readonly IJSRuntime js;

    public string ScreenshotPath =>screenshotPath;

    public ScreenshotCreator(IJSRuntime js)
    {
        this.js = js;
    }

    public void CleanUpFile()
    {
      if (!string.IsNullOrEmpty(screenshotPath) && File.Exists(screenshotPath))
      {
        File.Delete(screenshotPath);
      }
    }

    public async Task CreateScreenshotAsync(string predictorName)
    {        
        var imageName = predictorName + imageExtension;

        var dataReference = await js.InvokeAsync<IJSStreamReference>("createScreenshotImage");
        const long maxAllowedSizeInBytes = 10_000_000; // 10 MB
        using (var dataReferenceStream = await dataReference.OpenReadStreamAsync(maxAllowedSizeInBytes))
        {
          screenshotPath = Path.Combine(Path.GetTempPath(), imageName);
          using (var outputFileStream = File.OpenWrite(screenshotPath))
          {
            await dataReferenceStream.CopyToAsync(outputFileStream);
          }
          ResizeImage();
        }
    }

    public bool ResizeImage()
    {
      try
      {
        const int quality = 85;
        var resizedScreenshotPath = Path.Combine(
            Path.GetDirectoryName(screenshotPath),
            Path.GetFileNameWithoutExtension(screenshotPath) + "_resized" + Path.GetExtension(screenshotPath));

        using (var image = new MagickImage(screenshotPath))
        {
            image.Resize(new Percentage(CaclulateResizePercent(image.Width, image.Height)));
            image.Strip();
            image.Quality = quality;
            image.Write(resizedScreenshotPath);
        }
        
        // clean up the original screenshot image, to replace it with the resized image,
        CleanUpFile();

        // Rename the resized image to the original screenshot path.
        File.Move(resizedScreenshotPath, screenshotPath);
        return true;
      }
      catch (Exception)
      {
          throw;
      }
    }

    private int CaclulateResizePercent(int currentWidth, int currentHeight)
    {
        const int expectedWidth = 800;
        int resizePercent = 100 * expectedWidth / currentWidth;
        return resizePercent;
    }
}