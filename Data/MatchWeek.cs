using System.Collections.Generic;

public class MatchWeek : IMatchWeek
{
    public int matchWeekNumber { get; set; }
    public IList<Game> games { get; set; } = new List<Game>();
}
