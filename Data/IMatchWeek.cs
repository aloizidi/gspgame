using System.Collections.Generic;

public interface IMatchWeek
{
    public int matchWeekNumber{get; set;}
    public IList<Game> games {get; set;}
}