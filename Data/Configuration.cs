using System;

public static class Configuration
{
    private const string EnvironmentVariable = "ASPNETCORE_ENVIRONMENT";
    private const string ProductionEnvironment = "Production";
    private const string DBEnabled = nameof(DBEnabled);
    private const string API_URL = nameof(API_URL);

    public static bool IsProduction()
    {
       return Environment.GetEnvironmentVariable(EnvironmentVariable) == ProductionEnvironment;
    }
    
    public static bool IsDBEnabled()
    {
       var configValue = Environment.GetEnvironmentVariable(DBEnabled);
       return String.Compare(configValue, "true", StringComparison.OrdinalIgnoreCase) == 0;
    }
    
    public static string GetURLOfGspDatabase()
    {
       string configValue = Environment.GetEnvironmentVariable(API_URL);
       return configValue;
    }
    
}