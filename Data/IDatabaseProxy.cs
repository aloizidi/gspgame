using System.Threading.Tasks;
using System.Collections.Generic;

public interface IDatabaseProxy
{
    Task<MatchesOfTheWeekDTO> GetMatchesOfThisWeekAsync();
    Task<List<string>> GetlistOfPredictorsWhoPlayedAsync();

    Task<Dictionary<string, string>> GetlistOfPredictorsAsync();

    Task SetPredictionAsync(PredictionDTO PredictionDTO);
}