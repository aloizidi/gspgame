using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;

public class MatchReader : IMatchReader
{
    public MatchReader(IDatabaseProxy databaseProxy)
    {
        DatabaseProxy = databaseProxy;
    }

    private IDatabaseProxy DatabaseProxy { get; }

    public async Task<IMatchWeek> ReadMatchWeekAsync()
    {
        if (Configuration.IsDBEnabled())
        {
            return await ReadMatchesFromDBΑsync();
        }
        else
        {
            // At the moment the matchweek comes from the file directly. No db is used.
            return await Task.Run(()=> ReadMatchWeekFromFile());
        }
    }

    public async Task<IMatchWeek> ReadMatchesFromDBΑsync()
    {
        MatchesOfTheWeekDTO matchDTO = null;

        // Try to read the matches from the database
        try
        {
            matchDTO = await DatabaseProxy.GetMatchesOfThisWeekAsync();
        }
        catch(Exception e)
        {
            throw new Exception($"Cannot connect to DB, {e}");
        }

        return ConvertMatchDTOToMatchWeek(matchDTO);
    }

    public async Task<IMatchWeek> ReadFromDBIfNotReadFromfileΑsync()
    {
        IMatchWeek matchWeek = null;
        MatchesOfTheWeekDTO matchDTO = null;

        // Try to read the matches from the database
        try
        {
            matchDTO = await DatabaseProxy.GetMatchesOfThisWeekAsync();
        }
        catch(Exception) {}

        // If we could not connect to the database
        // or there are no games for the current week,
        // read the games from the json file.
        if (matchDTO == null || matchDTO?.Games?.Count == 0)
        {
            matchWeek = ReadMatchWeekFromFile();
        }
        else // update the matchWeek with the games from the database
        {
            matchWeek = ConvertMatchDTOToMatchWeek(matchDTO);
        }
        return matchWeek;      
    }
    
    public MatchWeek ReadMatchWeekFromFile()
    {
        string fileName = Path.Combine("wwwroot","TeamsLogos","MatchWeek.json");
        using FileStream openStream = File.OpenRead(fileName);
        return JsonSerializer.Deserialize<MatchWeek>(openStream);
    }

    private MatchWeek ConvertMatchDTOToMatchWeek(MatchesOfTheWeekDTO matchDTO)
    {
        MatchWeek matchWeek = new MatchWeek();
        matchWeek.matchWeekNumber = matchDTO.MatchWeekID;
        matchWeek.games = new List<Game>();
        foreach(var game in matchDTO.Games)
        {
            var newGame = new Game()
            {
                Team1 = game.HomeTeam,
                Team2 = game.GuestTeam,
                gameId = game.MatchID,
            };
        matchWeek.games.Add(newGame);
      }
      return matchWeek;
    }
}
