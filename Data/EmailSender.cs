using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;

public class EmailSender : IEmailSender
{
    private const string FromAddress = "thegspgame@gmail.com";
    private const string ToAddressGSP = "thegspgame@gmail.com";
    private const string ToAddressTest1 = "aloizidi@gmail.com";
    private const string ToAddressTest2 = "kostasloizidis@hotmail.com";
    private const string UserName = "thegspgame";
    private const string Password = "xmpgwxdawxldhbho";
    private const string Host = "smtp.gmail.com";

    

    public async Task SendEmailAsync(string predictorName, string pathToScreenshot, int matchWeekNumber, Dictionary<string,string> predictorsEmailAddresses)
    {
      var mailMessage = new MimeMessage();
      mailMessage.From.Add(new MailboxAddress("GSP game", FromAddress));
      mailMessage.To.AddRange(GetRecipientList(predictorName, predictorsEmailAddresses));
      mailMessage.Subject = $"Ο {predictorName} έστειλε την {matchWeekNumber}η αγωνιστική";

      var body = new TextPart("plain")
      {
          Text = $"Αγαπητή ομάδα Greek Superleague Prediction,\n\nΟ {predictorName} έστειλε του κουπόνι του για την {matchWeekNumber}η αγωνιστική.\n\nΜε χαρά,\nΟ παίχτης {predictorName}"
      };

      using(var file = File.OpenRead(pathToScreenshot))
      {
         // create an image attachment for the file located at path
        var attachment = new MimePart ("image", "jpg")
        {
          Content = new MimeContent (file, ContentEncoding.Default),
          ContentDisposition = new ContentDisposition (ContentDisposition.Attachment),
          ContentTransferEncoding = ContentEncoding.Base64,
          FileName = Path.GetFileName(pathToScreenshot)
        };

        // now create the multipart/mixed container to hold the message text and the
        // image attachment
        var multipart = new Multipart ("mixed");
        multipart.Add(body);
        multipart.Add(attachment);

        // now set the multipart/mixed as the message body
        mailMessage.Body = multipart;

        using (var smtpClient = new SmtpClient())
        {
            smtpClient.Connect(Host, 465, true);
            smtpClient.Authenticate(UserName, Password);
            await smtpClient.SendAsync(mailMessage);
            smtpClient.Disconnect(true);
        }
      }
    }

    private InternetAddressList GetRecipientList(string predictorName, Dictionary<string, string> predictorsEmailAddresses)
    {
      InternetAddressList list = new InternetAddressList();

      if (Configuration.IsProduction())
      {
        list.Add(new MailboxAddress("The GSP team", ToAddressGSP));
        list.Add(new MailboxAddress(predictorName, GetPredictorsEmailAddress(predictorName, predictorsEmailAddresses)));
      }
      else // It is development
      {
        list.Add(new MailboxAddress("The Test team", ToAddressTest1));
        list.Add(new MailboxAddress("The Test team", ToAddressTest2));
      }

      return list;
    }

    private string GetPredictorsEmailAddress(string predictorName, Dictionary<string, string> predictorsEmailAddresses)
    {
      if (predictorsEmailAddresses.ContainsKey(predictorName))
      {
        return predictorsEmailAddresses[predictorName];
      }
      else return string.Empty;
    }
}
