public class GameDTO
{
    public int MatchID { get; set; }
    public string HomeTeam { get; set;} = string.Empty;
    public string GuestTeam { get; set;} = string.Empty;
}