using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class Prediction : IPrediction
{
    public Prediction(IDatabaseProxy databaseProxy)
    {
        DatabaseProxy = databaseProxy;
    }

    private IDatabaseProxy DatabaseProxy { get; }

    public async Task<List<string>> GetlistOfPredictorsWhoPlayedAsync()
    {
        List<string> predictorsWhoPlayed = new List<string>();
        
        // Try to read the predictors from the database
        try
        {
            predictorsWhoPlayed = await DatabaseProxy.GetlistOfPredictorsWhoPlayedAsync();
        }
        catch(Exception e)
        {
            throw new Exception($"Cannot connect to DB, {e}");
        }
        return predictorsWhoPlayed;
    }


    public async  Task<Dictionary<string,string>> GetlistOfPredictorsAsync()
    {
         var predictors = new Dictionary<string,string>();
        
        // Try to read the predictors from the database
        try
        {
            predictors = await DatabaseProxy.GetlistOfPredictorsAsync();
        }
        catch(Exception e)
        {
            throw new Exception($"Cannot connect to DB, {e}");
        }
        return predictors;
    }

    public async Task SetPredictionAsync(PredictionDTO PredictionDTO)
    {
        try
        {
            await DatabaseProxy.SetPredictionAsync(PredictionDTO);
        }
        catch(Exception e)
        {
            throw new Exception($"Cannot connect to DB, {e}");
        }
    }
}