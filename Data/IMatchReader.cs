using System.Collections.Generic;
using System.Threading.Tasks;

public interface IMatchReader
{
    public Task<IMatchWeek> ReadMatchWeekAsync();
}