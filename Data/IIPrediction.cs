using System.Collections.Generic;
using System.Threading.Tasks;

public interface IPrediction
{
    Task<List<string>> GetlistOfPredictorsWhoPlayedAsync();
    Task SetPredictionAsync(PredictionDTO predictionDTO);
    Task<Dictionary<string,string>> GetlistOfPredictorsAsync();
}