using System.Collections.Generic;

public class PredictionDTO
{
    public string PredictorUsername { get; set; } = string.Empty;
    public IList<GamePredictionDTO> GamePredictions { get; set;} = new List<GamePredictionDTO>();
}