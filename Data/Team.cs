
public static class Team
{
    public enum TeamEnum
    {
        Unknown,
        Aek,
        Aris,
        Asteras,
        Atromitos,
        Kalithea,
        Kifisia,
        Lamia,
        Levadeiakos,
        Ofi,
        Olympiacos,
        Panaitolikos,
        Panathinaikos,
        Panserraikos,
        Paok,
        PasGiannina,
        Volos
    };

    public static string GetTeamLogo(TeamEnum teamEnum)
    {
        string teamLogoDir = @"TeamsLogos\";
        string teamImageFile = string.Empty;
        switch (teamEnum)
        {
            case TeamEnum.Aek:
                teamImageFile = "Aek.png";
                break;
            case TeamEnum.Aris:
                teamImageFile = "Aris.webp";
                break;
            case TeamEnum.Asteras:
                teamImageFile = "Asteras.png";
                break;
            case TeamEnum.Atromitos:
                teamImageFile = "Atromitos.png";
                break;
            case TeamEnum.Kalithea:
                teamImageFile = "Kalithea.png";
                break;
            case TeamEnum.Kifisia:
               teamImageFile = "Kifisia.png";
               break;
            case TeamEnum.Lamia:
                teamImageFile = "Lamia.webp";
                break;
            case TeamEnum.Levadeiakos:
                teamImageFile = "Levadeiakos.png";
                break;
            case TeamEnum.Ofi:
                teamImageFile = "Ofi.png";
                break;
            case TeamEnum.Olympiacos:
                teamImageFile = "Olympiacos.png";
                break;
            case TeamEnum.Panaitolikos:
                teamImageFile = "Panaitolikos.webp";
                break;
            case TeamEnum.Panathinaikos:
                teamImageFile = "Panathinaikos.png";
                break;
            case TeamEnum.Panserraikos:
                teamImageFile = "Panserraikos.png";
                break;
            case TeamEnum.Paok:
                teamImageFile = "Paok.png";
                break;
            case TeamEnum.PasGiannina:
                teamImageFile = "PasGiannina.png";
                break;
            case TeamEnum.Volos:
                teamImageFile = "Volos.png";
                break;
            case TeamEnum.Unknown:
            default:
                break;
        }
        return teamLogoDir + teamImageFile;
    }

    public static TeamEnum GetTeamEnum(string teamName)
    {
        switch (teamName)
        {
            case "Aek":
               return TeamEnum.Aek;
            case "Aris":
               return TeamEnum.Aris;
            case "Asteras":
               return TeamEnum.Asteras;
            case "Atromitos":
                return TeamEnum.Atromitos;
            case "Kalithea":
                return TeamEnum.Kalithea;
            case "Kifisia":
                 return TeamEnum.Kifisia;
            case "Lamia":
               return TeamEnum.Lamia;
            case "Levadeiakos":
               return TeamEnum.Levadeiakos;
            case "Ofi":
               return TeamEnum.Ofi;
            case "Olympiacos":
               return TeamEnum.Olympiacos;
            case "Panaitolikos":
               return TeamEnum.Panaitolikos;
            case "Panathinaikos":
               return TeamEnum.Panathinaikos;
            case "Panserraikos":
               return TeamEnum.Panserraikos;
            case "Paok":
               return TeamEnum.Paok;
            case "PasGiannina":
                return TeamEnum.PasGiannina;
            case "Volos":
               return TeamEnum.Volos;
            default: return TeamEnum.Unknown;
        }  
    }
    
    public static string GetTeamNameInGreek(TeamEnum teamEnum)
    {
        switch (teamEnum)
        {
            case TeamEnum.Aek:
               return "ΑΕΚ";
            case TeamEnum.Aris:
               return "Άρης";
            case TeamEnum.Asteras:
               return "Αστέρας";
            case TeamEnum.Atromitos:
                return "Ατρόμητος";
            case TeamEnum.Kalithea:
               return "Καλλιθέα";
            case TeamEnum.Kifisia:
               return "Κηφισιά"; 
            case TeamEnum.Lamia:
               return "Λαμία";
            case TeamEnum.Levadeiakos:
               return "Λεβαδειακός";
            case TeamEnum.Ofi:
               return "ΟΦΗ";
            case TeamEnum.Olympiacos:
               return "Ολυμπιακός";
            case TeamEnum.Panaitolikos:
               return "Παναιτωλικός";
            case TeamEnum.Panathinaikos:
               return "Παναθηναϊκός";
            case TeamEnum.Panserraikos:
               return "Πανσερραϊκός";
            case TeamEnum.Paok:
               return "ΠΑΟΚ";
            case TeamEnum.PasGiannina:
               return "ΠΑΣ Γιάννινα";
            case TeamEnum.Volos:
               return "Βόλος";
            case TeamEnum.Unknown:
            default:
               return "oops";
        }  
    }
}
