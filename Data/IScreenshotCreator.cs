using System.Threading.Tasks;

public interface IScreenshotCreator
{
    Task CreateScreenshotAsync(string predictorName);

    // Clean up temp file from the disk.
    void CleanUpFile();

    // Returns the full path on the disk where the sreenshot is stored.
    // This should be accessed after the CreateScreenshotAsync is called.
    string ScreenshotPath {get;}
}