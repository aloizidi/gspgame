using System;

public class Game
{
    public int gameId{get; set;}

    public string Team1 {get; set;}
    
    public string Team2 {get; set;}

    public string Score1 {get; set;}
    
    public string Score2 {get; set;}

}