using System.Collections.Generic;
using System.Linq;

public class FormState
{
    ResultFormState state = ResultFormState.Initial;

    // The action of changing the predictor
    public void PredictorSet(string predictorName)
    {
        var isPredictorNameValid = !string.IsNullOrEmpty(predictorName);
        var action = (isPredictorNameValid) ? ResultFormAction.SetValidName : ResultFormAction.SetInvalidName;
        UpdateState(action);
    }
    // The action of changing the score
     public void ScoresSet(List<string> scores)
    {
        var isScoreValid = scores.All(x=> !string.IsNullOrEmpty(x) && int.TryParse(x, out _));
        var action = (isScoreValid) ? ResultFormAction.SetValidScore : ResultFormAction.SetInvalidScore;
        UpdateState(action);
    }

    // The action of pressing the Send button
    public void SendButtonPressed()
    {
        UpdateState(ResultFormAction.Send);
    }

    public void SentSucceeded()
    {
        UpdateState(ResultFormAction.Succeeded);
    }

    public void SentFailed()
    {
        UpdateState(ResultFormAction.Failed);
    }

    public bool IsSendingInProgess()
    {
        return state == ResultFormState.Sending;
    }
    public bool ShouldShowMessage()
    {
        return !string.IsNullOrEmpty(GetMessage());
    }

    public bool ShouldShowSendButton()
    {
        return state == ResultFormState.AllInputValid ||
               state == ResultFormState.SentFailed;
    }

    public string GetMessage()
    {
        return messageStateDictionary[state];
    }

    private readonly Dictionary<ResultFormState,string> messageStateDictionary = 
        new Dictionary<ResultFormState, string>()
    {
        {ResultFormState.Initial,"Επιλέξτε Username πάνω πάνω"},
        {ResultFormState.UsernameValid,"Κάποιο σκορ είναι κενό ή λάθος"},
        {ResultFormState.ScoreValid,"Επιλέξτε Username πάνω πάνω"},
        {ResultFormState.AllInputValid,string.Empty},
        {ResultFormState.Sending,string.Empty},
        {ResultFormState.SentFailed,"Αποστολή απέτυχε. Δοκιμάστε ξανά"},
        {ResultFormState.SuccessfullySent,"Αποστολή επιτυχημένη"},
    };

    public bool ShouldShowMeMyLuck()
    {
        return state == ResultFormState.SuccessfullySent;
    }

    private void UpdateState(ResultFormAction action)
    {
        switch (state)
        {
            case ResultFormState.Initial:
                if (action == ResultFormAction.SetValidName)
                {
                    state = ResultFormState.UsernameValid;
                    break;
                }
                if (action == ResultFormAction.SetValidScore)
                {
                    state = ResultFormState.ScoreValid;
                    break;
                }
                break;
            case ResultFormState.UsernameValid:
                if (action == ResultFormAction.SetInvalidName)
                {
                    state = ResultFormState.Initial;
                    break;
                }
                if (action == ResultFormAction.SetValidScore)
                {
                    state = ResultFormState.AllInputValid;
                    break;
                }
                break;
            case ResultFormState.ScoreValid:
                if (action == ResultFormAction.SetValidName)
                {
                    state = ResultFormState.AllInputValid;
                    break;
                }
                if (action == ResultFormAction.SetInvalidScore)
                {
                    state = ResultFormState.Initial;
                    break;
                }
                break;
            case ResultFormState.AllInputValid:
            case ResultFormState.SentFailed:
                if (action == ResultFormAction.SetInvalidName)
                {
                    state = ResultFormState.ScoreValid;
                    break;
                }
                if (action == ResultFormAction.SetInvalidScore)
                {
                    state = ResultFormState.UsernameValid;
                    break;
                }
                if (action == ResultFormAction.Send)
                {
                    state = ResultFormState.Sending;
                    break;
                }                
                break;
            case ResultFormState.Sending:
                if (action == ResultFormAction.Failed)
                {
                    state = ResultFormState.SentFailed;
                    break;
                }
                if (action == ResultFormAction.Succeeded)
                {
                    state = ResultFormState.SuccessfullySent;
                    break;
                }
                break;
            case ResultFormState.SuccessfullySent:
                break;
            default:
                break;
        }
    }
}

// Actions
public enum ResultFormAction
{
    SetValidName,
    SetInvalidName,
    SetValidScore,
    SetInvalidScore,
    Send,
    Failed,
    Succeeded
}

// States
public enum ResultFormState
{
    Initial,
    UsernameValid,
    ScoreValid,
    AllInputValid,
    Sending,
    SentFailed,
    SuccessfullySent
}