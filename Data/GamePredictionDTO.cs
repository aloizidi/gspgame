public class GamePredictionDTO
{
    public GamePredictionDTO(){}
    public GamePredictionDTO(int matchID, int homeScore, int guestScore)
    {
        MatchID = matchID;
        HomeScore = homeScore;
        GuestScore = guestScore;
    }

    public int MatchID { get; set; }
    public int HomeScore { get; set; }
    public int GuestScore { get; set; }
}