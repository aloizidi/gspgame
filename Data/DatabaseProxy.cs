using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Net.Http.Headers;

public class DatabaseProxy : IDatabaseProxy
{    private static readonly HttpClient client = new HttpClient();

    public async Task<MatchesOfTheWeekDTO> GetMatchesOfThisWeekAsync()
    {
        string URL = Configuration.GetURLOfGspDatabase() + "/api/match/current";

        var response = await client.GetAsync(URL);

        if (response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            MatchesOfTheWeekDTO matchesOfTheWeekDTO = Deserializer(content);
            return matchesOfTheWeekDTO;
        }
        else
        {
            return new DatabaseProxyException("Cannot Load Matches from the database");
        }
    }

    public async Task<List<string>> GetlistOfPredictorsWhoPlayedAsync()
    {
        string URL = Configuration.GetURLOfGspDatabase() + "/api/predictorswhoplayed";

        var response = await client.GetAsync(URL);

        if (response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            List<string> predictorsWhoPlayed = JsonConvert.DeserializeObject<List<string>>(content);
            return predictorsWhoPlayed;
        }
        else
        {
            throw new Exception("Cannot Load predictors who played from the database");
        }
    }

     public async Task<Dictionary<string, string>> GetlistOfPredictorsAsync()
     {
        string URL = Configuration.GetURLOfGspDatabase() + "/api/predictors";
        var response = await client.GetAsync(URL);
        Dictionary<string, string> predictorsDictionary = new Dictionary<string, string>();

        if (response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            var predictors = JsonConvert.DeserializeObject<List<PredictorDTO>>(content);
            foreach (PredictorDTO predictor in predictors)
            {
                predictorsDictionary[predictor.Username] = predictor.EmailAddress;
            }

            return predictorsDictionary;
        }
        else
        {
            throw new Exception("Cannot Load predictors from the database");
        }

     }

    public Task SetPredictionAsync(PredictionDTO predictionDTO)
    {
        string URL = Configuration.GetURLOfGspDatabase() + "/prediction";

        using (var content = new StringContent(JsonConvert.SerializeObject(predictionDTO),
            System.Text.Encoding.UTF8, "application/json"))
        {
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage result = client.PostAsync(URL, content).Result;
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Task.CompletedTask;
            }
            else
            {
                string returnValue = result.Content.ReadAsStringAsync().Result;
                throw new Exception($"Failed to POST data: ({result.StatusCode}): {returnValue}");
            }
        }
    }

    private MatchesOfTheWeekDTO Deserializer(string JSON) 
    {
        return JsonConvert.DeserializeObject<MatchesOfTheWeekDTO>(JSON);
    }
}
