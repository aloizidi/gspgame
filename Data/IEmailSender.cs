using System.Collections.Generic;
using System.Threading.Tasks;

public interface IEmailSender
{
    Task SendEmailAsync(string predictorName, string pathToScreenshot, int matchWeekNumber, Dictionary<string, string> predictorsEmailAddresses);
}