using System.Collections.Generic;

public class MatchesOfTheWeekDTO
{
    public IList<GameDTO> Games { get; set;} = new List<GameDTO>();
    public int MatchWeekID { get; set;}
}
