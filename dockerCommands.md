
VS code:

docker build --rm --pull -f "C:\Users\aloiz\DotNetProjects\gspgame\gspgame/Dockerfile" --label "com.microsoft.created-by=visual-studio-code" -t "gspgame:dev" --target "base" "C:\Users\aloiz\DotNetProjects\gspgame\gspgame"

docker run -dt -P --name "gspgame-dev" -e "DOTNET_USE_POLLING_FILE_WATCHER=1" -e "ASPNETCORE_ENVIRONMENT=Development" --label "com.microsoft.created-by=visual-studio-code" -v "C:\Users\aloiz\DotNetProjects\gspgame\gspgame:/app:rw,z" -v "c:\Users\aloiz\DotNetProjects\gspgame\gspgame:/src:rw,z" -v "C:\Users\aloiz\.vsdbg:/remote_debugger:ro,z" -v "C:\Users\aloiz\.nuget\packages:/root/.nuget/packages:ro,z" -v "C:\Users\aloiz\.nuget\packages:/home/appuser/.nuget/packages:ro,z" "gspgame:dev"

Staging
docker build -t gspgame:6 .
docker run -d --rm -e "DBEnabled=true" -e "ASPNETCORE_ENVIRONMENT=Development" -p 5000:5000 gspgame:6

Production
docker build -t gspgame:6 .
docker run -d --rm -e "ASPNETCORE_ENVIRONMENT=Production" -p 80:5000 gspgame:6

Docker Compose
docker-compose -f "docker-compose.debug.yml" up -d --build
docker-compose -f docker-compose.yml -f docker-compose.staging.yml up -d

docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' 339f6c64b8c1